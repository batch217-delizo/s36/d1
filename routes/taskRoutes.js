//It contains all endpoints for our application
const express = require("express");
const router =  express.Router();
const taskController = require("../controllers/taskController.js");

//Section Routes
//It is responsible for defining or creating endpoints
//All the business logic is done in the controllers

router.get("/viewTasks", (req, res) => {
  taskController.getAllTasks().then(resultFromController => res.send(
    resultFromController ));
})

router.post("/addNewTasks", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => {
      res.send(resultFromController);
    })
})

router.put("/updateTask/:id", (req, res) => {
  taskController.updateTask(req.params.id, req.body).then(resultFromController => {
    res.send(resultFromController);
  })
})

router.delete("/deleteTask/:id", (req, res) => {
  taskController.deleteTask(req.params.id).then(resultFromController => {
    res.send(resultFromController);
  })
})
module.exports = router;